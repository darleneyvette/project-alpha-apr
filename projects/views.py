from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from projects.models import Project
from projects.forms import ProjectForm


# Create your views here.
@login_required
def project_list_view(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"project_list_view": projects}
    return render(request, "projects/list.html", context)


@login_required
def project_detail_view(request, id):
    project = get_object_or_404(Project, id=id)
    context = {"project_detail_view": project}
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            form.save()
            return redirect("/projects/")
    else:
        form = ProjectForm()
    context = {"project_form": form}
    return render(request, "projects/create_project.html", context)
