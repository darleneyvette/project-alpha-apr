from django.contrib import admin
from tasks.models import Task

# Register the Task model with the admin so you can see it in the Django admin site. You want to do this in the tasks Django app's admin.py.


admin.site.register(Task)
